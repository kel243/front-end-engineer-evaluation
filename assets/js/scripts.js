var tempBtns = document.getElementsByClassName('temp-btn');
var key = "c72b3f689d4adead6c18f5d3747020e6";

var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
var weekdayShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

$(document).ready(function() {
    document.getElementById('search-btn').addEventListener('click', function(event) {
        var input = document.getElementById('search-bar').value.split(',');
        getWeatherData(input);
        $('#search-bar').val('');
    });

    for (var i = 0; i < tempBtns.length; i++) {
        tempBtns[i].addEventListener('click', function(event) {
            for (var i = 0; i < tempBtns.length; i++) {
                tempBtns[i].classList.remove('active');
            }
            var id = event.target.id;
            document.getElementById(id).classList.add('active');
        })
    }
});

function getWeatherData(input) {
    var lat = parseFloat(input[0]);
    var lon = parseFloat(input[1]);
    var url = "https://api.openweathermap.org/data/2.5/onecall?lat=" + lat + "&lon=" + lon + "&exclude=hourly&appid=" + key + "&units=imperial";
    // Write a function to use the DarkSky api to gather weather data and update the screen.
  
    fetch(url)
    .then(function(response) {
        // handle the response
        return response.json();
    })
    .then(function(json) {
        var sunrise = json.current.sunrise;
        var sunset = json.current.sunset;
        var current = json.current.dt;

        var dailies = json.daily;
        console.log(json.current.weather[0].main)

        // Render today's forecast and date
        $('#today-temperature').html(parseInt(json.current.temp) + " °F");
        $('#main-icon').html('<i class="fas ' + getIcon(json.current.weather[0].main) + ' weather-icon-large"></i>');
        $('#today-date').html(getWeekDay(current, 'long') + ', ' + formatDate(current));

        // Render 7 day forecast
        $('#day-1').html(getWeekDay(dailies[0].dt, 'short'));
        $('#day-temperature-1').html(parseInt(dailies[0].temp.day) + " / " + parseInt(dailies[0].temp.night));
        $('#weather-icon-1').html('<i class="fas ' + getIcon(dailies[0].weather[0].main) + ' weather-icon-small"></i>');
        $('#day-2').html(getWeekDay(dailies[1].dt, 'short'));
        $('#day-temperature-2').html(parseInt(dailies[1].temp.day) + " / " + parseInt(dailies[1].temp.night));
        $('#weather-icon-2').html('<i class="fas ' + getIcon(dailies[1].weather[0].main) + ' weather-icon-small"></i>');
        $('#day-3').html(getWeekDay(dailies[2].dt, 'short'));
        $('#day-temperature-3').html(parseInt(dailies[2].temp.day) + " / " + parseInt(dailies[2].temp.night));
        $('#weather-icon-3').html('<i class="fas ' + getIcon(dailies[2].weather[0].main) + ' weather-icon-small"></i>');
        $('#day-4').html(getWeekDay(dailies[3].dt, 'short'));
        $('#day-temperature-4').html(parseInt(dailies[3].temp.day) + " / " + parseInt(dailies[3].temp.night));
        $('#weather-icon-4').html('<i class="fas ' + getIcon(dailies[3].weather[0].main) + ' weather-icon-small"></i>');
        $('#day-5').html(getWeekDay(dailies[4].dt, 'short'));
        $('#day-temperature-5').html(parseInt(dailies[4].temp.day) + " / " + parseInt(dailies[4].temp.night));
        $('#weather-icon-5').html('<i class="fas ' + getIcon(dailies[4].weather[0].main) + ' weather-icon-small"></i>');
        $('#day-6').html(getWeekDay(dailies[5].dt, 'short'));
        $('#day-temperature-6').html(parseInt(dailies[5].temp.day) + " / " + parseInt(dailies[5].temp.night));
        $('#weather-icon-6').html('<i class="fas ' + getIcon(dailies[5].weather[0].main) + ' weather-icon-small"></i>');
        $('#day-7').html(getWeekDay(dailies[6].dt, 'short'));
        $('#day-temperature-7').html(parseInt(dailies[6].temp.day) + " / " + parseInt(dailies[6].temp.night));
        $('#weather-icon-7').html('<i class="fas ' + getIcon(dailies[6].weather[0].main) + ' weather-icon-small"></i>');

        // Render today's highlights
        $('#visibility').html((json.current.visibility * 0.0006213712).toFixed(2) + " mi");
        $('#humidity').html(json.current.humidity + '%');
        $('#dew').html(parseInt(json.current.dew_point) + '%');
        $('#uv').html(json.current.uvi);
        $('#wind').html(json.current.wind_speed + " <span id='mph'>mph</span>");
        $('#sun').addClass('today-highlight-info-sun');
        $('#sun').html('<i class="fas fa-arrow-circle-up arrow-sun"></i> ' + formatDate(sunrise) + '<br>' + '<i class="fas fa-arrow-circle-down arrow-sun"></i> ' + formatDate(sunset));
        
    })
    .catch(function(error) {
        // handle the error
        console.log(error)
    });
    // Optionally, if you would rather use a library as was discussed in the README, feel free to do so.
}

// Format date into "Hour:Minute Period" format
function formatDate(unix) {
    var date = new Date(unix * 1000);
    var hour = date.getHours();
    var minutes = "0" + date.getMinutes();
    var period = '';
    if (hour > 12) {
        hour = hour - 12;
        period = 'PM';
    } else {
        period = 'AM';
    }

    return hour + ":" + minutes.substr(-2) + ' ' + period;
}


// Convert numerical week day to textual week day
function getWeekDay(unix, mode) {
    var date = new Date(unix * 1000);
    var day = date.getDay();

    if(mode === 'long') {
        return weekday[day];
    } else if (mode === 'short') {
        return weekdayShort[day];
    }
}

// Get icon based on weather
function getIcon(weather) {
    switch(weather) {
        case 'Clear':
            return 'fas fa-sun'
        case 'Clouds':
            return 'fas fa-cloud'
        case 'Rain':
            return 'fas fa-cloud-rain'
        case 'Snow':
            return 'fas fa-snowflake'
    }
}

